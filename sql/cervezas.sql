DROP DATABASE IF EXISTS cervezas;
CREATE DATABASE cervezas;
USE cervezas;
CREATE TABLE cervezas(
  id int auto_increment primary key,
  name varchar(255),
  description varchar(255),
  alcohol varchar(255),
  container varchar(255),
  price double
);

insert into cervezas(name, description, alcohol, container, price)
VALUES(
    "DAMM INEDIT",
    "Creada por los cerveceros de Damm junto a Ferrán Adriá y los sumilleres de El Bulli, se elabora con una mezcla de malta de cebada y trigo aromatizada con cilantro, piel de naranja y regaliz.",
    "4,8º",
    "Botella de 75cl",
    "3.90"
),(
    "ALHAMBRA 1925",
    "Con su característica botella de color verde, se trata de una cerveza extra con una graduación de 6,8º. Se distingue por su peculiar toque acaramelado y por su perfecto y refrescante amargor final.Gran cuerpo y mucho equilibrio.",
    "6,8º",
    "Botella de 33 cl.",
    "1"
), (
    "MAHOU CINCO ESTRELLAS",
    "Un auténtico clásico entre nuestras cervezas. Muy ligera y agradable, resulta especialmente refrescante. Con una ligera acidez, sabor a cebada tostada y un correcto amargor final.",
    "5,5º",
    "Pack de 6 botellines de 25 cl.",
    "2.70"
), (
    "SAN MIGUEL 1516",
    "Entre las varias opciones de esta marca, una de las más internacionales de las españolas, destaca esta 1516 elaborada según métodos tradicionales. Fresca, amarga y con baja graduación, lo que la hace muy agradable a cualquier hora.",
    "4,2º",
    "Botella de 33 cl.",
    "0.80"
), (
    "CRUZ CAMPO GRAN RESERVA 1904",
    "Cerveza cien por cien malta, de gran calidad. Intensa y equilibrada, con agradable final amargo. graduación muy adecuada para acompañar cualquier tipo de comida.",
    "6,4º",
    "Botella de 33cl",
    "1"
), (
    "VOLL DAMM",
    "Elaborada con el doble de malta, lo que le proporciona un sabor muy característico, y un cuerpo intenso y peculiar, diferente de otras. Alta graduación alcohólica. Adecuada para tomar como copa.",
    "7,2º",
    "Botella de 33cl",
    "1.10"
), (
    "ÁMBAR ESPECIAL",
    "La Zaragozana es una centenaria fábrica de cervezas de la capital zaragozana. Ofrece una amplia variedad. La más atractiva es esta Especial, una lager de baja fermentación que resulta muy fácil y agradable de beber.",
    "5,2º",
    "Botella de 33cl",
    "0.90"
), (
    "MORITZ",
    "Una cerveza casi artesanal, que es una institución en Barcelona desde 1856. En su elaboración se emplean agua de un manantial de Vichy Catalán y flores de lúpulo en lugar de extractos, lo que le confiere más aroma y menos amargor.",
    "5,4º",
    "Botella de 33cl",
    "1.20"
), (
    "BRABANTE BLANCA",
    "Elaborada en Bélgica con métodos tradicionales, se puede considerar española pues la hacen empresarios madrileños para el mercado nacional. De sus variedades destaca esta blanca de trigo, ligera y suave.",
    "5º",
    "Botella de 33cl",
    "1.50"
)